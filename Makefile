.PHONY: all clone update clean

all: clone update

clone:
	@echo "Cloning verwalte"
	git clone --branch $${VERWALTE_BRANCH:-master} git@bitbucket.org:intevation/verwalte.git
	@echo "Cloning ball-lightning"
	git clone --branch $${BALL_LIGHTNING_BRANCH:-master} git@bitbucket.org:intevation/ball-lightning.git

update:
	@echo "Update verwalte"
	@cd verwalte && git pull
	@echo "Update ball-lightning"
	@cd ball-lightning && git pull

clean:
	@echo "Cleaning ..."
	rm -rf ball-lightning verwalte
