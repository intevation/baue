# Baue

Build enviroment for docker to setup a partial or full stack blids universe.

- [Baue](#baue)
  - [Requirements](#requirements)
  - [Clone required repositories](#clone-required-repositories)
    - [Update required repositories to their latest revision](#update-required-repositories-to-their-latest-revision)
  - [Automatically build and run your dev application using `docker-compose`](#automatically-build-and-run-your-dev-application-using-docker-compose)
    - [Optional: Using enviroment variables with `docker-compose`](#optional-using-enviroment-variables-with-docker-compose)
    - [Hints and tricks for `docker-compose`](#hints-and-tricks-for-docker-compose)
  - [Manually build and run your dev application](#manually-build-and-run-your-dev-application)
    - [Build docker images](#build-docker-images)
      - [Optional: Using enviroment variables with `docker build`](#optional-using-enviroment-variables-with-docker-build)
    - [Create a network](#create-a-network)
    - [Container startup](#container-startup)
    - [Insert demo data](#insert-demo-data)
  - [Tips and Tricks](#tips-and-tricks)
    - [Connect to running database container](#connect-to-running-database-container)

## Requirements

- Cloned repositories
- [Docker](https://docker.com) installed in a fairly actual version

Requires at least Docker Engine release 17.12.0+. Because of Compose file
format 3.5 for named networks.

## Clone required repositories

```shell
make
```

`make` will clone the following required repositories.

- <https://bitbucket.org/intevation/verwalte>
- <https://bitbucket.org/intevation/ball-lightning>

### Update required repositories to their latest revision

```shell
make clean
```

## Automatically build and run your dev application using `docker-compose`

Run the `build` commands from the root of your checkout, because it is
assumed as the context!

Other example commands, too, assume they are run from the root of your
checkout.

If you fiddle around with the following error message: `Version in
"./docker-compose.yml" is unsupported` take a look at the comments in the
head of the `docker-compose.yml` file.

From your docker directory, start up your application by running
`docker-compose up`. Here an example for build and running the application:

```shell
DB_CONNECTION=XXXXXXXXXXXXXXXXXXXXXXXXXXXX docker-compose -p ball-lightning up
```

The `$DB_CONNECTION` environment variable is a placeholder for the database
connection string. The connection string has the following format:

```shell
echo -n 'u=$REPLACE_WITH_USER pw=$REPLACE_WITH_PW h=ball-lightning-db d=$REPLACE_WITH_DB_NAME' | base64
```

Note: The `-p` flag specifies an alternate project name (default: directory
name).

Now you should be able to use the application in your browser by going to
`http://HOSTNAME:8080`, with `HOSTNAME` being the hostname of your docker
host.

Stop the application, either by running `docker-compose -p ball-lightning down`
from within your docker directory in the second terminal, or by hitting
`CTRL+C` in the original terminal where you started the application.

### Optional: Using enviroment variables with `docker-compose`

There exist optional enviroment variables for the mount host paths (repos)
for the coresponding database, backend and client docker container:

- `VOL_DB_REPO`
- `VOL_BACKEND_REPO`
- `VOL_CLIENT_REPO`

Example for using these enviroment variables to start the application:

```shell
DB_CONNECTION=XXXXXXXXXXXXXXXXXXXXXXXXXXXX VOL_CLIENT_REPO=<PATH>/<TO>/<CLIENT_REPO> docker-compose -p ball-lightning up
```

### Hints and tricks for `docker-compose`

If you want to run your services in the background, you can pass the `-d`
flag (for `detached` mode) to `docker-compose -p ball-lightning up` and use
`docker-compose -p ball-lightning ps` to see what is currently running. Take
a look at the following example.

```shell
docker-compose -p ball-lightning up -d
Creating network "lightning" with the default driver
Creating ball-lightning-db ... done
Creating ball-lightning-backend ... done
Creating ball-lightning-client  ... done
```

```shell
docker-compose -p ball-lightning ps
Name                       Command               State            Ports
-----------------------------------------------------------------------------------------
ball-lightning-backend   /bin/sh -c CompileDaemon - ...   Up      0.0.0.0:5000->5000/tcp
ball-lightning-client    docker-entrypoint.sh /opt/ ...   Up      0.0.0.0:8080->8080/tcp
ball-lightning-db        /usr/lib/postgresql/11/bin ...   Up      0.0.0.0:54321->5432/tcp
```

For more infos about Docker Compose take a look at:
<https://docs.docker.com/compose/>

## Manually build and run your dev application

### Build docker images

It is assumed that the repositories are all in this directory.

```shell
docker build -t ball-lightning-db -f Dockerfile.db .
docker build -t ball-lightning-backend -f Dockerfile.backend .
docker build -t ball-lightning-client -f Dockerfile.spa .
```

#### Optional: Using enviroment variables with `docker build`

There exist optional enviroment variables for copying the repos for the
coresponding database, backend and client docker images:

- `COPY_DB_REPO`
- `COPY_BACKEND_REPO`
- `COPY_CLIENT_REPO`

Example for using these enviroment variables to build a docker database image:

```shell
COPY_DB_REPO=<PATH>/<TO>/<DB_REPO> docker run --name ball-lightning-db -d -p 54321:5432 --network lightning ball-lightning-db
```

### Create a network

Create a new docker network for the containers.

```shell
docker network create lightning
```

Further information:
<https://docs.docker.com/engine/reference/commandline/network_create/>

### Container startup

Start the containers using the created network and expose the SPA port. Use
the [volume mounts](https://docs.docker.com/storage/volumes/) to include the
local repositories for development.

```shell
docker run --name ball-lightning-db -d -p 54321:5432 --network lightning ball-lightning-db
docker run --name ball-lightning-backend -v ball-lightning:/go/src/bitbucket.org/intevation/ball-lightning -dt \
  -p 5000:5000 --network lightning -e DB_CONNECTION=$DB_CONNECTION ball-lightning-backend
docker run --name ball-lightning-client -v $PWD/ball-lightning/client:/opt/ball-lightning/client -td -p 8080:8080 --network lightning ball-lightning-client
```

The `$DB_CONNECTION` is a palceholder for the database connection string. For
the container the connection string has the following format:

```shell
echo -n 'u=$REPLACE_WITH_USER pw=$REPLACE_WITH_PW h=ball-lightning-db d=$REPLACE_WITH_DB_NAME' | base64
```

### Insert demo data

To insert the demo data from the `verwalte` repository just run:

```shell
docker exec -ti ball-lightning-db psql -d blids -f demo-data.sql
```

## Tips and Tricks

### Connect to running database container

```shell
docker exec -it ball-lightning-db psql -d blids
```
