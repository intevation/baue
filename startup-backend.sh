#! /bin/sh
export TAG=$(git tag --points-at HEAD)
export GIT_COMMIT=${TAG:-$(git rev-parse --short HEAD)}
cd /go/src/bitbucket.org/intevation/ball-lightning/cmd/lightning && go build  -ldflags="-X 'bitbucket.org/intevation/ball-lightning/pkg/version.Version=$GIT_COMMIT'" && ./lightning & /bin/sh
